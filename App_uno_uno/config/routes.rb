Rails.application.routes.draw do
    get 'sessions/create'
    get 'sessions/destroy'
	get 'about' => 'pages#about' ,as: :about
	get 'contact' => 'pages#contact' ,as: :contact
	root to: 'posts#index'
  	resources :posts
    get 'auth/:provider/callback', to: 'sessions#create'
    get 'auth/failure', to: redirect('/')
    get 'signout', to: 'sessions#destroy', as: 'signout'

    resources :sessions, only: [:create, :destroy]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
