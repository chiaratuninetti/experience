Rails.application.routes.draw do

  resources :posts
  get 'Blog' => 'pages#Blog' ,as: :Blog
  get 'Profile' => 'pages#Profile', as: :Profile
  get 'ricerca' => 'pages#ricerca', as: :ricerca
  get 'index' => 'posts#index' ,as: :index
  get 'Info' => 'pages#Info', as: :Info
  get 'Contact' => 'pages#Contact', as: :Contact

  get 'auth/:provider/callback', to: 'sessions#create'
  get 'auth/failure', to: redirect('/')
  get 'signout', to: 'sessions#destroy', as: 'signout'

  resources :sessions, only: [:create, :destroy]

  root to: 'welcome#homepage'
  resources :welcome


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
