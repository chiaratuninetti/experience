require 'test_helper'

class PagesControllerTest < ActionDispatch::IntegrationTest
  test "should get Blog" do
    get pages_Blog_url
    assert_response :success
  end

  test "should get Profile" do
    get pages_Profile_url
    assert_response :success
  end

end
