class CreatePosts < ActiveRecord::Migration[5.2]
  def change
    create_table :posts do |t|
      t.string :title
      t.string :body_text
      t.text :description
      t.string :slug

      t.timestamps
    end
  end
end
